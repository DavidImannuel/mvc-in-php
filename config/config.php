<?php 

define('BASEURL', 'http://localhost/mvc-in-php/');
define('ASSET', 'http://localhost/mvc-in-php/public/');

function baseurl($url=''){
    return BASEURL.$url;
}
function asset($url=''){
    return ASSET.$url;
}

//DB
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_NAME', 'phpmvc');
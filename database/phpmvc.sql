-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 16, 2019 at 07:15 AM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpmvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `example`
--

CREATE TABLE `example` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `example`
--

INSERT INTO `example` (`id`, `name`, `email`, `city`, `phone`) VALUES
(248, 'Mechelle Hancock', 'sem.ut.dolor@Integersemelit.edu', 'Moulins', '(648) 223-8400'),
(249, 'Rebekah Pennington', 'aliquet@Aliquamrutrumlorem.edu', 'Manoppello', '(417) 171-7918'),
(250, 'Xena Lyons', 'arcu.eu.odio@Inscelerisque.edu', 'Etobicokek', '(496) 813-0714'),
(251, 'Nola Becker', 'justo.eu.arcu@nisiCum.edu', 'Amstelveen', '(330) 376-3343'),
(252, 'Raya Alvarado', 'Phasellus.at.augue@arcuNunc.co.uk', 'Oberwart', '(642) 174-4297'),
(253, 'Christen Grimes', 'eleifend.nunc@Inornare.net', 'Sommariva Perno', '(595) 354-5367'),
(254, 'Alea Mendez', 'volutpat.Nulla@Aliquamtinciduntnunc.com', 'Livo', '(514) 545-8210'),
(255, 'Lucy Fuentes', 'velit.eget@velarcu.com', 'Porirua', '(963) 714-6240'),
(256, 'Quyn Case', 'magna@iaculisquis.ca', 'St. Thomas', '(318) 644-3137'),
(257, 'Patricia Dalton', 'cursus.et.magna@sodalesatvelit.net', 'San Vito Chietino', '(613) 486-7420'),
(258, 'Kimberly Black', 'molestie.tellus.Aenean@egetvenenatis.net', 'Leernes', '(664) 275-0476'),
(259, 'Montana Evans', 'lobortis@convallisligulaDonec.net', 'Crewe', '(923) 388-2902'),
(260, 'Penelope Cameron', 'aliquam@Pellentesqueultriciesdignissim.com', 'Tielen', '(791) 820-9228'),
(261, 'Mira Gregory', 'nec.diam.Duis@ligulaelit.ca', 'Sciacca', '(247) 591-4776'),
(262, 'Sophia Fischer', 'convallis.erat.eget@Integersemelit.net', 'Ruddervoorde', '(745) 954-8751'),
(263, 'Olga Gaines', 'Cras.vulputate.velit@lobortistellus.org', 'Dresden', '(509) 405-5744'),
(264, 'Paloma Langley', 'lacus.Quisque@velconvallisin.net', 'San Felice a Cancello', '(608) 207-0312'),
(265, 'Iliana Chandler', 'aliquet.nec@Donecvitae.co.uk', 'Pictou', '(433) 638-2480'),
(266, 'Jena Manning', 'Integer.urna.Vivamus@Nullainterdum.com', 'Lacombe', '(236) 142-6753'),
(267, 'Willow Hansen', 'fringilla.porttitor@aaliquetvel.com', 'San Valentino in Abruzzo Citeriore', '(143) 626-7108'),
(268, 'Lenore Levy', 'et@ut.ca', 'Bear', '(760) 329-9476'),
(269, 'Odette Jefferson', 'sit.amet.diam@magnaSedeu.ca', 'Kirkland', '(766) 126-0129'),
(270, 'Willow Austin', 'gravida@pharetrasedhendrerit.co.uk', 'Cetara', '(700) 112-9333'),
(271, 'Kyra Rosales', 'et@acmieleifend.com', 'Greenlaw', '(380) 770-1250'),
(272, 'Rana Patel', 'arcu.eu.odio@tinciduntadipiscingMauris.co.uk', 'Moere', '(709) 376-3861'),
(273, 'Alexandra Gillespie', 'ut@Proinegetodio.net', 'Gaasbeek', '(803) 354-1362'),
(274, 'Ginger Mccarthy', 'dignissim.Maecenas@lacusQuisque.ca', 'Mold', '(461) 641-6200'),
(275, 'Fay Roth', 'Duis.sit@pharetrafeliseget.org', 'Lethbridge', '(709) 569-9028'),
(276, 'Evelyn Jimenez', 'non@sollicitudin.net', 'Vieux-Genappe', '(496) 761-5861'),
(277, 'Sopoline Miranda', 'ac.facilisis.facilisis@Maurisblanditenim.com', 'Berwick-upon-Tweed', '(851) 267-7609'),
(278, 'Selma Chavez', 'nunc.sit@felis.edu', 'Vernole', '(432) 502-7268'),
(279, 'Stacey Contreras', 'a@enim.co.uk', 'Roccamena', '(204) 216-0210'),
(280, 'Veronica Henry', 'aliquet.molestie@accumsanconvallisante.net', 'Huppaye', '(134) 519-2774'),
(281, 'Rhea Turner', 'nulla@eratvolutpat.org', 'Cefalà Diana', '(732) 775-8803'),
(282, 'Montana Morrison', 'Maecenas.mi.felis@Nuncquisarcu.edu', 'Essen', '(838) 182-2900'),
(283, 'Summer Wilkinson', 'Aliquam.nisl.Nulla@vitaeposuereat.com', 'Bergeggi', '(128) 447-3460'),
(284, 'April Harding', 'eros.turpis.non@aliquamenimnec.ca', 'Cochamó', '(821) 532-6157'),
(285, 'Breanna Sloan', 'dis.parturient@disparturient.ca', 'Nimy', '(230) 692-2524'),
(286, 'Eden Zamora', 'tristique.senectus@Morbiquisurna.com', 'Bad Ischl', '(762) 113-2690'),
(287, 'Karleigh Maddox', 'Aliquam@non.ca', 'ZŽtrud-Lumay', '(824) 381-6537'),
(288, 'Kristen Moon', 'tellus@volutpatornare.org', 'Heusden', '(216) 702-0431'),
(289, 'Erin Hood', 'Integer.in@nislQuisque.ca', 'Sevilla', '(430) 823-5687'),
(290, 'Nora Lucas', 'mauris.eu@sapienNunc.com', 'Chañaral', '(317) 229-9524'),
(291, 'Molly Marsh', 'amet.ultricies.sem@felisorci.com', 'Carbonear', '(351) 543-7176'),
(292, 'Katell England', 'amet@Utnecurna.co.uk', 'Sankt Wendel', '(969) 571-0426'),
(293, 'Rhoda Harrington', 'lobortis@nonleoVivamus.ca', 'Gölcük', '(643) 835-1258'),
(294, 'Alisa Hubbard', 'euismod.ac@gravidasagittis.edu', 'Challand-Saint-Victor', '(423) 135-0092'),
(295, 'Yen Wyatt', 'iaculis.quis.pede@elitNulla.edu', 'Lathuy', '(859) 463-7906'),
(296, 'Jada Dyer', 'ipsum@lobortis.co.uk', 'Mont-Saint-AndrŽ', '(382) 183-6496'),
(297, 'Savannah Hahn', 'mi.Aliquam@Utsagittislobortis.com', 'Sunset Point', '(656) 709-3838'),
(298, 'Sopoline Hines', 'ligula.elit.pretium@posuerecubiliaCurae.edu', 'Weert', '(654) 220-6330'),
(299, 'Eugenia Alvarez', 'sodales.elit.erat@eleifend.edu', 'Olathe', '(952) 118-7625'),
(300, 'Phyllis Knox', 'semper.pretium.neque@nostraper.net', 'Oud-Turnhout', '(950) 365-4173'),
(301, 'Isabelle Strickland', 'scelerisque.neque.sed@luctuslobortis.com', 'Hanret', '(115) 388-4434'),
(302, 'Lana Potter', 'Sed.nulla@Aeneanegestas.co.uk', 'Milton Keynes', '(856) 439-7486'),
(303, 'Claudia Hood', 'aliquet@auguescelerisquemollis.ca', 'Norcia', '(109) 755-8818'),
(304, 'Germane Monroe', 'Quisque@nonnisiAenean.org', 'Ávila', '(447) 882-5121'),
(305, 'Phoebe Sanders', 'libero.mauris.aliquam@ametmetus.co.uk', 'Largs', '(801) 296-1368'),
(306, 'Lara Ellison', 'blandit@etnetuset.com', 'Pomarolo', '(570) 672-7491'),
(307, 'Julie Rich', 'arcu@gravidanunc.ca', 'Lutsel K\'e', '(321) 565-9191'),
(308, 'Debra Moore', 'lobortis.tellus@Maecenas.net', 'Longvilly', '(998) 218-4761'),
(309, 'Angela Sargent', 'sapien@dis.ca', 'Pittsburgh', '(274) 447-8826'),
(310, 'Daphne Webster', 'convallis@justoProin.org', 'Pellizzano', '(879) 335-1391'),
(311, 'Nell Flynn', 'ut.erat.Sed@vulputatelacus.org', 'St. John\'s', '(133) 776-6157'),
(312, 'Venus Fields', 'adipiscing.enim.mi@eleifend.co.uk', 'Pietrarubbia', '(789) 954-7333'),
(313, 'Sloane Barber', 'nisl@Phasellus.ca', 'Esterzili', '(268) 616-8098'),
(314, 'Rebecca Harrell', 'ultricies.adipiscing.enim@necmalesuadaut.net', 'Knokke-Heist', '(192) 661-3492'),
(315, 'Hannah Jenkins', 'tempor.lorem@ligulaAliquamerat.edu', 'Schleswig', '(495) 393-6002'),
(316, 'Cassady Delgado', 'tincidunt.Donec@ut.org', 'Northumberland', '(726) 671-7410'),
(317, 'Dana Robinson', 'Proin@Vivamusmolestie.org', 'Río Ibáñez', '(821) 435-1152'),
(318, 'Bell Bray', 'Nunc.commodo@quamquisdiam.ca', 'Glovertown', '(845) 299-1856'),
(319, 'Cara Collins', 'semper.egestas.urna@Nam.com', 'Chambord', '(949) 918-6147'),
(320, 'Roary Stuart', 'at.pede@malesuadafamesac.com', 'Ucluelet', '(940) 100-2278'),
(321, 'Karyn Ayers', 'Proin@Donecfelis.net', 'Montacuto', '(970) 856-1754'),
(322, 'Sarah Gay', 'tempus.risus.Donec@dapibus.org', 'Castro', '(573) 784-4941'),
(323, 'Hadley Ortega', 'neque.tellus.imperdiet@odio.ca', 'Sint-Kruis', '(506) 605-8220');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `example`
--
ALTER TABLE `example`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `example`
--
ALTER TABLE `example`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=324;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

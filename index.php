<?php 
    //------------------------------------------//
   //------- Simple MVC concept in PHP --------//
  //----IG @david_imannuel -------------------//
 //----GitLab DavidImannuel -----------------//
//------------------------------------------//
define('ROOT',__DIR__);
// var_dump(ROOT);
require_once ROOT.'/config/config.php';
require_once ROOT.'/app/core/autoload.php';

session_start();

$route = new Route();



<?php 

class Controller {

	protected $db;

	public function __construct()
	{
		$this->db = DB::connect();
	}

    public function view($view,$data = [])
	{
		require_once ROOT.'/app/views/'.$view.'.php';
	}

	public function model($model)
	{
		require_once ROOT.'/app/models/'.$model.'.php';
		// $obj = 'App\Models\\'.$model;
		return new $model;
	}


}
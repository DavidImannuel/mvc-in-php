<?php 

class DB {

    static private $host = DB_HOST;
    static private $user = DB_USER;
    static private $pass = DB_PASS;
    static private $db_name = DB_NAME;
	static private $pdo;

    public static function connect()
    {
        $dsn = 'mysql:host='.self::$host.';dbname='.self::$db_name;
        try {
            self::$pdo = new PDO($dsn,self::$user,self::$pass);
        } catch(PDOException $e) {
            die($e->getMessage());
		}
        return self::$pdo;
    }


}